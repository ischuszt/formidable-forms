import { BrowserRouter, Route, Routes } from "react-router-dom";
import Root from "./pages/Root";
import SimpleForm from "./pages/SimpleForm";
import HookForm from "./pages/HookForm";
import ActivityForm from "./pages/ActivityForm/ActivityForm";
import ActivityFormStep2 from "./pages/ActivityFormStep2/ActivityFormStep2";
function App() {

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route element={<Root />}>
            <Route index path="/" element={<SimpleForm />} />
            <Route path="/hookform" element={<HookForm />} />
            <Route path="/activityform" element={<ActivityForm onSubmit={(data) => console.log(data)} />} />
            <Route path="/activityformstep2" element={<ActivityFormStep2 />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
