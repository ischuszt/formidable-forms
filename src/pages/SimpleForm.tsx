import { Button, Typography } from "@mui/material";
import { useState } from "react";

const formStyles = {
  input: {
    marginTop: "10px",
  },
};

function SimpleForm() {
  const [formState, setFormState] = useState({
    firstName: "",
    lastName: "",
  });

  const [displayForm, setDisplayForm] = useState("");

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormState({ ...formState, [e.target.name]: e.target.value });
  };

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setDisplayForm(JSON.stringify(formState));
  };

  return (
    <>
      <Typography variant="h5">Simple controlled form</Typography>
      <form
        onSubmit={onSubmit}
        style={{ display: "flex", flexDirection: "column", maxWidth: "350px" }}
      >
        <input
          style={formStyles.input}
          type="text"
          value={formState.firstName}
          name="firstName"
          required
          onChange={onChange}
          placeholder="First name..."
        />
        <input
          style={formStyles.input}
          type="text"
          value={formState.lastName}
          name="lastName"
          onChange={onChange}
          placeholder="Last name..."
        />
        <Button variant="contained" sx={{ m: 2 }} type="submit">
          Submit
        </Button>
      </form>
      {displayForm && <pre>Submitted: {displayForm}</pre>}
    </>
  );
}

export default SimpleForm;
