import { FormProvider, useForm } from "react-hook-form";
import HookFormSelector from "../../components/HookFormSelector";
import { activityTypes } from "../../types/activityTypes";
import { yupResolver } from "@hookform/resolvers/yup";
import { Activity, useActivityFormSchema } from "./useActivityFormSchema";
import { Button } from "@mui/material";
import HookFormTextField from "../../components/HookFormTextField";
import { useNavigate } from "react-router-dom";
import HookFormYesNoRadio from "../../components/HookFormYesNoRadio";

export const ActivityFormInitialFields = () => {
  return (
    <>
    <HookFormSelector
    name="activityType"
    label="Activity type"
    disabled={false}
    selectorItems={activityTypes}
  />
  <HookFormYesNoRadio
    name="isBookable"
    label="Is bookable?"
  />
  </>
  )
}


function ActivityForm({ onSubmit }: { onSubmit: (data: Activity) => void }) {
  const schema = useActivityFormSchema();
  const navigate = useNavigate()

  const methods = useForm<Activity>({
    resolver: yupResolver(schema),
  });

  const onClickSubmit = (data: Activity) => {
    onSubmit(data)
    navigate('/activityformstep2', { state: data })
  };

  return (
    <>
      <FormProvider {...methods}>
        <ActivityFormInitialFields />
      </FormProvider>
      <Button sx={{mt: 4}} variant="contained" onClick={methods.handleSubmit(onClickSubmit)}>
        Submit
      </Button>
    </>
  );
}

export default ActivityForm;
