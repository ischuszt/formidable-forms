import { Matcher, fireEvent, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ActivityForm from "./ActivityForm";
import { MemoryRouter } from "react-router-dom";

const selectValueInSelector = async (value: Matcher, label: Matcher) => {
  const selector = await screen.findByLabelText(label);
  fireEvent.mouseDown(selector.childNodes[0]);
  await userEvent.click(await screen.findByText(value));
};

describe("ActivityForm", () => {
  const fakeSubmit = vi.fn();

  const renderActivityForm = () => {
    render(
      <MemoryRouter>
        <ActivityForm onSubmit={fakeSubmit} />
      </MemoryRouter>
    );
  };

  it("should render and submit", async () => {
  renderActivityForm();
    await selectValueInSelector(/lab workshop/i, /activity type/i);

    const noRadioButton = await screen.findByText('No')
    await userEvent.click(noRadioButton)

    const submitButton = await screen.findByText(/submit/i);
    await userEvent.click(submitButton);
    expect(fakeSubmit).toHaveBeenCalledOnce();
  });
});
