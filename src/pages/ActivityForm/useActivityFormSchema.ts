import { ObjectSchema, boolean, object, string } from "yup";

export type Activity = {
  activityType: string;
  isBookable: boolean;
};

export const useActivityFormSchema = (): ObjectSchema<Activity> => {
  return object({
    activityType: string().required("This field is required"),
    isBookable: boolean().required('You need to select a value')
  });
};
