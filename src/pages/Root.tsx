import { Container } from "@mui/material";
import { Link, Outlet } from "react-router-dom";
import "./Root.css";

function Root() {
  return (
    <div id="wrapper">
      <div id="sidebar">
        <ul>
          <li>
            <Link to="/">Simple Form</Link>
          </li>
          <li>
            <Link to="/hookform">Hook form</Link>
          </li>
          <li>
            <Link to="/activityform">Activity form</Link>
          </li>
        </ul>
      </div>
      <Container>
        <Outlet />
      </Container>
    </div>
  );
}

export default Root;
