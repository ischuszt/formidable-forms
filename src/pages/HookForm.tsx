import { Box, Button } from "@mui/material";
import { useState } from "react";
import { useForm } from "react-hook-form";

type ContactInfo = {
  firstName: string;
  lastName: string;
};

function HookForm() {
  const [data, setData] = useState<string>();

  const {
    handleSubmit,
    register,
    formState: { errors, isDirty, isValid },
  } = useForm<ContactInfo>();


  const onSubmit = (payload: unknown) => {
    setData(JSON.stringify(payload));
  };
  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Box sx={{ display: "flex", flexDirection: "column" }}>
          <label>
            First Name:
            <input
              id="firstName"
              {...register("firstName", { required: true })}
            />
            {errors.firstName && (
              <p style={{ color: "red" }}>This field is required</p>
            )}
          </label>
          <label>
            Last Name:
            <input id="lastName" {...register("lastName")} />
          </label>
          <Button
            sx={{ m: 2, mt: 4 }}
            variant="contained"
            onClick={handleSubmit(onSubmit)}
          >
            Submit
          </Button>
        </Box>
      </form>
      {data && <pre>{data}</pre>}
      <pre>valid: {JSON.stringify(isValid)}</pre>
      <pre>dirty: {JSON.stringify(isDirty)}</pre>
    </>
  );
}

export default HookForm;
