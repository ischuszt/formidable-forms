import { useLocation } from "react-router-dom";
import {
  Activity,
  useActivityFormSchema,
} from "../ActivityForm/useActivityFormSchema";
import { FormProvider, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button } from "@mui/material";
import { ActivityFormInitialFields } from "../ActivityForm/ActivityForm";
import HookFormTextField from "../../components/HookFormTextField";
import { object, string } from "yup";

type ExtendedActivity = {
  title: string;
  descriptionEn: string;
  descriptionFr: string;
} & Activity;

function ActivityFormStep2() {
  const schema = useActivityFormSchema();
  const myNewSchema = schema.concat(
    object({
      title: string().required("This field is required"),
      descriptionEn: string().required("This field is required"),
      descriptionFr: string().required("This field is required"),
    })
  );

  const { state } = useLocation();
  const myActivityState = state as Activity;

  const onClickSubmit = (data: ExtendedActivity) => {
    console.log(data);
  };

  const methods = useForm<ExtendedActivity>({
    defaultValues: { ...myActivityState },
    resolver: yupResolver(myNewSchema),
  });

  return (
    <>
      <FormProvider {...methods}>
        <ActivityFormInitialFields />
        <HookFormTextField name="title" label="Title" />
        <HookFormTextField name="descriptionEn" label="Description EN" />
        <HookFormTextField name="descriptionFr" label="Description FR" />
      </FormProvider>

      <Button
        sx={{ mt: 4 }}
        variant="contained"
        onClick={methods.handleSubmit(onClickSubmit)}
      >
        Submit
      </Button>
    </>
  );
}

export default ActivityFormStep2;
