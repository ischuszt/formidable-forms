import { SxProps } from "@mui/system";

export const fieldStyles: SxProps = {
  marginLeft: "1.5rem",
  marginRight: "1.5rem",
  marginTop: "2rem",
};
