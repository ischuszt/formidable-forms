type SelectorValues<T> = { id: T; value: string };

export default SelectorValues;
