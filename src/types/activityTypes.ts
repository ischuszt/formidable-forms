import SelectorValues from "./SelectorValues";

export const activityTypes: SelectorValues<string>[] = [
  { id: "TOUR", value: "Guided tour" },
  { id: "LAB", value: "Lab workshop" },
  { id: "EXHIBITION", value: "Exhibition" },
];
