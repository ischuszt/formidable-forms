import {
  FormControl,
  FormHelperText,
  FormLabel,
  MenuItem,
  Select,
  TextFieldVariants,
} from "@mui/material";
import { Box, SxProps } from "@mui/system";
import { useMemo } from "react";
import {
  Controller,
  FieldValues,
  Path,
  get,
  useFormContext,
} from "react-hook-form";
import SelectorValues from "../types/SelectorValues";
import { fieldStyles } from "../utils/hookFormUtils";

export type HookFormSelectorProps<T extends FieldValues, V> = {
  name: Path<T>;
  selectorItems: SelectorValues<V>[];
  label: string;
  helperText?: string;
  disabled?: boolean;
  sx?: SxProps;
  variant?: TextFieldVariants;
  onChange?: () => void;
};

function generateSelectorFormItems<T>(
  array: SelectorValues<T>[],
): JSX.Element[] {
  return array.map((reason) => (
    <MenuItem key={reason.id + ""} value={reason.id as string}>
      {reason.value}
    </MenuItem>
  ));
}

/**
 * A dropdown selector for use with react-hook-form.
 */
function HookFormSelector<T extends FieldValues, V>({
  name,
  label,
  helperText,
  disabled,
  selectorItems,
  variant,
  sx,
  onChange,
}: HookFormSelectorProps<T, V>) {
  const {
    formState: { errors },
    control,
  } = useFormContext<T>();

  const options = useMemo(() => {
    return generateSelectorFormItems<V>(selectorItems);
  }, [selectorItems]);

  const error = get(errors, name);

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <Box sx={sx ?? fieldStyles}>
          <FormControl
            error={!!error}
            variant={variant || "outlined"}
            fullWidth
          >
            <FormLabel id={`selector-form-label-${name}`}>{label}</FormLabel>
            <Select
              aria-labelledby={`selector-form-label-${name}`}
              disabled={disabled}
              {...field}
              fullWidth
              label={label}
              variant={variant || "outlined"}
              error={!!error}
              MenuProps={{ PaperProps: { sx: { maxHeight: "75%" } } }}
              onChange={(event) => {
                field.onChange(event);
                if (onChange != null) {
                  onChange();
                }
              }}
            >
              {options}
            </Select>
            <FormHelperText>
              {error ? (error?.message as string) : helperText}
            </FormHelperText>
          </FormControl>
        </Box>
      )}
    />
  );
}

export default HookFormSelector;
