import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup, SxProps } from "@mui/material";
import { Controller, FieldValues, Path, get, useFormContext } from "react-hook-form";

type HookFormYesNoRadioProps<T extends FieldValues> = {
  name: Path<T>;
  label: string;
  sx?: SxProps;
};

function HookFormYesNoRadio<T extends FieldValues>({
  name,
  label,
  sx,
}: HookFormYesNoRadioProps<T>) {
  const {
    formState: { errors },
    control,
  } = useFormContext<T>();
  const error = get(errors, name);

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => 
        <FormControl error={!!errors[name]} variant="filled" fullWidth>
            <FormLabel>{label}</FormLabel>
            <RadioGroup value={field.value} onChange={field.onChange}>
              <FormControlLabel
                value={true}
                control={<Radio size="small" />}
                label={'Yes'}
              />
              <FormControlLabel
                value={false}
                control={<Radio size="small" />}
                label={'No'}
              />
              <span>{error && (error?.message as string)}</span>
            </RadioGroup>
      </FormControl>}
    />
  );
}

export default HookFormYesNoRadio;
