import { Box, SxProps, TextField, TextFieldVariants } from "@mui/material";
import { HTMLInputTypeAttribute } from "react";
import {
  Controller,
  FieldValues,
  Path,
  get,
  useFormContext,
} from "react-hook-form";
import { fieldStyles } from "../utils/hookFormUtils";

export type HookFormTextFieldProps<T extends FieldValues> = {
  name: Path<T>;
  type?: HTMLInputTypeAttribute;
  label: string;
  textarea?: boolean;
  variant?: TextFieldVariants;
  helperText?: string;
  sx?: SxProps;
  placeholder?: string;
  size?: "small" | "medium";
};

function HookFormTextField<T extends FieldValues>({
  name,
  type,
  textarea,
  label,
  helperText,
  variant,
  placeholder,
  size,
  sx,
}: HookFormTextFieldProps<T>) {
  const {
    formState: { errors },
    control,
  } = useFormContext<T>();

  const error = get(errors, name);

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { ref, ...field } }) => (
        <Box sx={sx ?? fieldStyles}>
          <TextField
            {...field}
            inputRef={ref}
            size={size || "medium"}
            label={label}
            multiline={textarea}
            rows={textarea ? 4 : undefined}
            type={type}
            placeholder={placeholder}
            variant={variant || "outlined"}
            fullWidth
            error={!!error}
            helperText={error ? (error?.message as string) : helperText}
          />
        </Box>
      )}
    />
  );
}

export default HookFormTextField;
