# Formidable forms

React Norway 2024 - supporting code

## Running

```
npm install
npm run dev
```

## Testing

```
npm test
```

or

```
npm run test:ui
```

if you want to see a nice UI for your tests.